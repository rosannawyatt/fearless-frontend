function createCard(title, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
          ${startDate} - ${endDate}
        </div>
      </div>
    `;
  }

function createAlert(){
  return `
    <div class="alert alert-danger d-flex align-items-center" role="alert">
      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
      <div>
        Request failed
      </div>
    </div>
  `
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        let i = 0
        const columns = document.querySelectorAll('.col');
        if (!response.ok) {
          const alert = createAlert()
          const alertTag = document.querySelector('.col')
          alertTag.innerHTML = alert
        } else {
            const data = await response.json();
            
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const startDateISO = new Date(details.conference.starts);
                    const endDateISO = new Date(details.conference.ends);
                    const startDate = (startDateISO.getMonth()+1) + "/" + startDateISO.getDate() + "/" + startDateISO.getFullYear()
                    const endDate = (endDateISO.getMonth()+1) + "/" + endDateISO.getDate() + "/" + endDateISO.getFullYear();
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                    const cardCol = i % 3;
                    i += 1;
                    columns[cardCol].innerHTML += html;
                }  
            }

        }
    } catch (e){
        console.error('error')
        const alert = createAlert()
          const alertTag = document.querySelector('.col')
          alertTag.innerHTML = alert
    }

});
