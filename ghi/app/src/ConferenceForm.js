import React, { useEffect, useState } from 'react';

function ConferenceForm (props) {
    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setLocations(data.locations)
        };
    }

    useEffect(() => {
        fetchData();
    }, []);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [startDate, setStartDate] = useState('');
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }
    const [endDate, setEndDate] = useState('');
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const [maxPresentations, setMaxPresentations] = useState(0);
    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const [maxAttendees, setMaxAttendees] = useState(0);
    const handleMaxAttendChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const [location, setLocation] = useState(0);
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setStartDate('');
          setEndDate('');
          setDescription('');
          setMaxPresentations('');
          setMaxAttendees('');
          setLocation('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Conference name" required type="text" name="name" className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartDateChange} placeholder="MM/DD/YYYY" required type="date" name="starts" className="form-control" value={startDate} />
                            <label htmlFor="starts">Start Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndDateChange} placeholder="MM/DD/YYYY" required type="date" name="ends" className="form-control" value={endDate} />
                            <label htmlFor="ends">End Date</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea onChange={handleDescriptionChange} placeholder="Conference description" className="form-control" name="description" rows="3" value={description}></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresChange} placeholder="0" required type="number" name="max_presentations" className="form-control" value={maxPresentations} />
                            <label htmlFor="max_presentations">Maximum allowed presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendChange} placeholder="0" required type="number" name="max_attendees" className="form-control" value={maxAttendees} />
                            <label htmlFor="max_attendees">Maximum allowed attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} name="location" required id="location" className="form-select" value={location} >
                            <option default value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                )
                            }

                            )}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
      </div>
    )
}

export default ConferenceForm;